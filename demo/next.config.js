/** @format */

module.exports = {
	basePath: '/demo',
	future: {
		webpack5: true,
	},
};
