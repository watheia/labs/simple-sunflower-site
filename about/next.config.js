/** @format */

const { ABOUT_URL } = process.env

module.exports = {
  basePath: "/about",
  future: {
    webpack5: true,
  },
}
